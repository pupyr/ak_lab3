import pytest
from main2 import start

@pytest.mark.test("golden/*yml")
def test_find_words(golden):
    start()
    golden = golden.open("golden/fibbi.yml")

    with open("code", encoding="utf-8") as file:
        code = file.read()
    assert code == golden.out["out-code"]
    with open("data", encoding="utf-8") as file:
        data = file.read()
    assert data == golden.out["out-data"]
    with open("log", encoding="utf-8") as file:
        log = file.read()
    assert log == golden.out["out-log"]

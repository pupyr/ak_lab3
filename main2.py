def tokens():
    return {"in", "out", "+", "-", ">", "<", "==", "=", "for", "nope", "if", "%"}

addrCode = []
addrData = []
log = []


def mem(token):
    # добавить определение кол-ва места
    com = token.split()
    assert com[0] in tokens(), "Wrong token"
    assert com[0] != "=" or ord(com[1]) >= 65, "Wrong var name"
    if com[0] == "nope" and addrCode[int(com[1][2:])].split()[1] == "if":
        last = addrCode[int(com[1][2:])].rfind(" ") + 1
        addrCode[int(com[1][2:])] = addrCode[int(com[1][2:])][:last] + "0x" + str(len(addrCode))
    for j in range(1, len(com)):
        if ord(com[j][0]) >= 65:
            b = True
            ind = 0
            for j2 in range(len(addrData)):
                if addrData[j2].split()[1] == com[j]:
                    b = False
                    ind = str(j2)
                    break
            if b:
                assert j == 1, "Unknown var"
                ind = str(len(addrData))
                addrData.append("1x"+ind+": "+com[1]+" "+com[2])
            token = token.replace(com[j], "1x"+ind)
    addrCode.append("0x" + str(len(addrCode)) + ": " + token)
    if com[0] == "for":
        addr = len(addrCode)-1
        while "0x" in addrCode[addr][2:]:
            if "0x" in addrCode[addr].split()[2]:
                addr = int(addrCode[addr].split()[2][2:])
            elif "0x" in addrCode[addr].split()[3]:
                addr = int(addrCode[addr].split()[3][2:])
        var = addrCode[len(addrCode)-1].rfind(" ")
        addrCode[len(addrCode)-1] = addrCode[len(addrCode)-1][:var] + " 0x" + str(addr)


def get_token(source):
    a = source.find("(", 1)
    while a+1:
        b = a+1
        count = 1
        while count:
            if source[b] == "(":
                count += 1
            elif source[b] == ")":
                count -= 1
            b += 1
        get_token(source[a:b])
        source = source.replace(source[a:b], "0x"+str(len(addrCode)-1))
        a = source.find("(", a)
    else:
        mem(source[1:-1])


def ret_oper(com, scr):
    if "x" not in com:
        return int(com)
    elif "0x" in com:
        op = int(addrData[scr + int(com[2:])].split()[2])
    else:
        op = int(addrData[int(com[2:])].split()[2])
    return op


# ra - reg address
# ro - reg operation
# rb - reg buf

def player(scr):
    ra = 0
    while addrCode[ra].split()[1] != "HALT":
        com = addrCode[ra].split()
        assert len(com) == 4, "Wrong count of operands"
        ro = com[1]
        acc = ret_oper(com[2],scr)
        rb = ret_oper(com[3],scr)
        if ro == "+":
            acc = acc+rb
        elif ro == "-":
            acc = acc-rb
        elif ro == ">":
            acc = int(acc > rb)
        elif ro == "<":
            acc = int(acc < rb)
        elif ro == "%":
            acc = acc % 2
        elif ro == "==":
            acc = int(acc == rb)
        elif ro == "=":
            last = addrData[int(com[2][2:])].rfind(" ")+1
            acc = rb
            addrData[int(com[2][2:])] = addrData[int(com[2][2:])][:last]+str(acc)
        elif ro == "out":
            try:
                port = open(acc, "r+")
                acc = rb
                port.write(str(acc))
            except FileNotFoundError:
                print("порт "+str(acc)+" не подключен")
                exit(0)
        elif ro == "in":
            try:
                port = open(acc, "r")
                rb = port.read()
                acc = rb
                last = addrData[int(com[3][2:])].rfind(" ")+1
                addrData[int(com[3][2:])] = addrData[int(com[3][2:])][:last]+acc
            except FileNotFoundError:
                print("порт "+str(acc)+" не подключен")
                exit(0)
        elif ro == "for":
            assert "x" in com[2], "wrong for"
            if acc != 0:
                ra = int(com[3][2:])-1
        elif ro == "if":
            if not acc:
                ra = int(com[3][2:])
        last = addrData[scr + int(com[0][2:-1])].rfind(" ") + 1
        addrData[scr + int(com[0][2:-1])] = addrData[scr + int(com[0][2:-1])][:last] + str(acc)
        log.append([com[0][:-1], com[2], com[3], acc, rb, ra, ro])
        ra += 1

def trans(source):
    ind = 0
    count = 0
    while ind < len(source):
        if source[ind] == "(":
            count += 1
        elif source[ind] == ")":
            count -= 1
        ind += 1
        assert count != -1, "Wrong bracket"
    assert count == 0, "Wrong bracket"
    source = source.replace("\n", "")
    get_token(source)


def start():
    scr = 0
    f = open("input")
    c = open("code", "w")
    d = open("data", "w")
    l = open("log", "w")
    for i in f.readlines():
    #for i in f.split("\n"):
    #    if i!="":
        trans(i)
    addrCode.append("0x" + str(len(addrCode)) + ": HALT")
    scr = len(addrData)
    for i in range(len(addrCode)):
        addrData.append("1x" + str(scr + i) + ": FUNC 0")
        c.write(addrCode[i]+"\n")
    player(scr)
    result = ""
    for i in addrData:
        d.write(i+"\n")
        result += i+"\n"
    l.write("///////////////////////////////////////////////////////////////////////\n")
    l.write("///////////////////////////////////////////////////////////////////////\n")
    l.write("|addr code|   op1   |   op2   |   acc   |   r b   |   r a   |   r o   |\n")
    l.write("///////////////////////////////////////////////////////////////////////\n")
    l.write("///////////////////////////////////////////////////////////////////////\n")
    for i in log:
        s = "|"
        for j in i:
            n = 9-len(str(j))
            for j2 in range(n):
                s += ' '
            s += str(j)+"|"
        l.write(s+"\n")
        l.write("-----------------------------------------------------------------------\n")
    return result


# file = open("input")
# sr = ""
# for i in file.readlines():
#     sr += i
# start(sr)
